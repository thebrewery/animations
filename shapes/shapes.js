document.querySelectorAll('.shapes__radio-button').forEach((el) => {
	el.addEventListener('click', () => {
		document.querySelectorAll('.shape-img').forEach((el) => {
			el.classList.remove('shapes__active')
			el.style.opacity = '0'
			el.style.transition = 'none'
		})

		const foundRelative = document.querySelector(`img#${el.id}`)
		foundRelative.classList.add('shapes__active')
		foundRelative.style.opacity = '1'
		foundRelative.style.transition =
			'opacity 0.25s cubic-bezier(0.25, 0.1, 0.25, 1)'

		const marginToggler = document.querySelector('.grid-shape')
		if (marginToggler.checked) {
			const marginRelative = document.querySelector(
				`img#${foundRelative.id}-margin`
			)

			marginRelative.style.opacity = '1'
			marginRelative.style.transition =
				'opacity 0.25s cubic-bezier(0.25, 0.1, 0.25, 1)'
		}
	})
})

document.querySelectorAll('.grid-shape').forEach((el) => {
	el.addEventListener('click', () => {
		if (document.querySelector('.shapes__active')) {
			/* first we check if a image is active, and check if checked or not */
			const activeElement = document.querySelector('.shapes__active')
			const foundRelative = document.querySelector(
				`img.${activeElement.classList[0]}-margin`
			)

			if (el.checked) {
				foundRelative.style.opacity = '1'
				foundRelative.style.transition =
					'opacity 0.25s cubic-bezier(0.25, 0.1, 0.25, 1)'
			} else {
				foundRelative.style.opacity = '0'
			}
		}
	})
})
