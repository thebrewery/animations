const allGridElements = document.getElementsByClassName('split__item')

const videoImageContainer = document.querySelector(
	'.split__item-image-container'
)
const playButtonContainer = document.querySelector(
	'.split__item-play-container'
)
const playButton = document.getElementById('play-button')
const playButtonOnHover = document.getElementById('play-button-hover')
const videoPlayer = document.getElementById('video')
const playBackContainer = document.getElementById('playback-container')

document.querySelector('.split__item-video').addEventListener('click', (e) => {
	e.preventDefault()
})

playButton.addEventListener('mousemove', function () {
	this.classList.add('split__hidden-element')
	playButtonOnHover.classList.remove('split__hidden-element')
})

playButtonOnHover.addEventListener('mouseleave', function () {
	this.classList.add('split__hidden-element')
	playButton.classList.remove('split__hidden-element')
})

playButtonOnHover.addEventListener('click', function (e) {
	e.stopPropagation()
	if (videoPlayer.paused) {
		videoPlayer.play()
		playBackContainer.classList.remove('split__hidden-element')
		videoImageContainer.classList.add('split__hidden-content')
		videoPlayer.style.display = 'block'
	}
})

playBackContainer.addEventListener('click', function (e) {
	e.stopPropagation()
	if (videoPlayer.paused) {
		this.classList.remove('split__fade-in--opacity')
		videoPlayer.play()
		playBackContainer.classList.remove('split__hidden-element')
		videoImageContainer.classList.add('split__hidden-content')
		videoPlayer.style.display = 'block'
	}
})

Array.from(allGridElements).forEach((el) => {
	let videoText = document.getElementById('split__video-text')

	el.addEventListener('click', (clickEvent) => {
		if (!videoPlayer.paused) {
			videoPlayer.pause()
			videoPlayer.style.display = 'none'
			playBackContainer.classList.add('split__fade-in--opacity')
			videoImageContainer.classList.remove('split__hidden-content')

			return
		}

		if (el.classList.contains('split__animated')) {
			el.classList.toggle('split__animated')

			playButtonContainer.classList.remove('split__fade-in--opacity')
			videoText.classList.remove('split__hidden-element')

			setTimeout(() => {
				videoText.classList.remove('split__fade-out--opacity')
			}, 500)
		} else {
			Array.from(allGridElements).forEach((el) =>
				el.classList.remove('split__animated')
			)
			el.classList.add('split__animated')

			setTimeout(() => {
				playButtonContainer.classList.add('split__fade-in--opacity')
			}, 500)
			videoText.classList.add('split__fade-out--opacity')

			setTimeout(() => {
				videoText.classList.add('split__hidden-element')
			}, 600)
		}

		setTimeout(() => {
			el.style.zIndex = 1
		}, 900)
		el.style.zIndex = 3
	})
})
