document.querySelectorAll('.expressions-wrapper').forEach((mainContainer) => {
	const prevButton = mainContainer.querySelector('.expressions__button--prev')
	const nextButton = mainContainer.querySelector('.expressions__button--next')

	let itemClassName = 'expressions__photo',
		items = mainContainer.getElementsByClassName(itemClassName),
		totalItems = items.length,
		slide = 0,
		moving = true

	// Set classes
	function setInitialClasses() {
		// Targets the previous, current, and next items
		// This assumes there are at least three items.
		items[totalItems - 1].classList.add('prev')
		items[0].classList.add('active')
		items[1].classList.add('next')
	}

	// Set event listeners
	function setEventListeners() {
		var next = mainContainer.getElementsByClassName(
				'expressions__button--next'
			)[0],
			prev = mainContainer.getElementsByClassName(
				'expressions__button--prev'
			)[0]
		next.addEventListener('click', moveNext)
		prev.addEventListener('click', movePrev)
	}

	// Next navigation handler
	function moveNext() {
		// Check if moving
		if (!moving) {
			// If it's the last slide, reset to 0, else +1
			if (slide === totalItems - 1) {
				slide = 0
			} else {
				slide++
			}
			// Move carousel to updated slide
			moveCarouselTo(slide, 'next')
		}
	}

	// Previous navigation handler
	function movePrev() {
		// Check if moving
		if (!moving) {
			// If it's the first slide, set as the last slide, else -1
			if (slide === 0) {
				slide = totalItems - 1
			} else {
				slide--
			}

			// Move carousel to updated slide
			moveCarouselTo(slide, 'prev')
		}
	}

	function disableInteraction() {
		// Sets 'moving' to true, during the animation duration

		moving = true
		setTimeout(function () {
			moving = false
			prevButton.disabled = true
			nextButton.disabled = true
			setTimeout(() => {
				prevButton.disabled = false
				nextButton.disabled = false
			}, 300)
		}, 600)
	}

	function moveCarouselTo(slide, direction) {
		if (!moving) {
			// temporarily disable interactivity
			disableInteraction()

			// Update the "old" adjacent slides with "new" ones
			var newPrevious = slide - 1,
				newNext = slide + 1,
				oldPrevious = slide - 2,
				oldNext = slide + 2

			// Check if carousel has 3 or more items, else exit
			if (totalItems - 1 >= 3) {
				if (newPrevious <= 0) {
					oldPrevious = totalItems - 1
				} else if (newNext >= totalItems - 1) {
					oldNext = 0
				}

				// Checks and updates if slide is at the beginning/end
				if (slide === 0) {
					newPrevious = totalItems - 1
					oldPrevious = totalItems - 2
					oldNext = slide + 1
				} else if (slide === totalItems - 1) {
					newPrevious = slide - 1
					newNext = 0
					oldNext = 1
				}

				items[oldPrevious].className = itemClassName
				items[oldNext].className = itemClassName

				// Add new classes
				items[slide].className = itemClassName + ' expressions__animating'

				if (direction === 'prev') {
					items[newNext].className =
						itemClassName + ' expressions__animating-away'
				} else {
					items[newPrevious].className =
						itemClassName + ' expressions__animating-away'
				}

				setTimeout(() => {
					items[newPrevious].className = itemClassName + ' prev'
					items[slide].className = itemClassName + ' active'
					items[newNext].className = itemClassName + ' next'
				}, 600)
			}
		}
	}

	function initCarousel() {
		setInitialClasses()
		setEventListeners()
		moving = false
	}

	initCarousel()
})
