const legalText = document.querySelector('.versions__legal')

document.querySelectorAll('.versions__blue-logotype-btn').forEach((el) =>
	el.addEventListener('click', () => {
		if (el.classList.contains('versions__active')) return

		document.querySelectorAll('.versions__active').forEach((el) => {
			el.textContent = ''
			el.classList.remove('versions__active')
		})

		document.querySelectorAll('.versions__buttons').forEach((laterEl) => {
			if (laterEl !== el) laterEl.style.width = '40px'
		})

		const whiteLogo = document.querySelector('.versions__white-logotype-btn')
		const blackLogo = document.querySelector('.versions__black-logotype-btn')
		whiteLogo.style.left = '180px'

		if (el.offsetWidth !== 160) {
			el.style.left = '0'
			el.style.width = '160px'
			blackLogo.style.left = 0
		}

		el.textContent = 'Blue logotype'
		el.classList.add('versions__active')

		/* image handling */
		document
			.querySelectorAll('.versions__image')
			.forEach((el) => (el.style.opacity = 0))

		const relativeImage = document.querySelector('#versions__blue-image')
		relativeImage.style.opacity = 1

		legalText.textContent =
			'The Blue logotype should be used when it is placed on top of the Skanska Grey, photography or film.'
	})
)

document.querySelectorAll('.versions__white-logotype-btn').forEach((el) =>
	el.addEventListener('click', () => {
		if (el.classList.contains('versions__active')) return

		document.querySelectorAll('.versions__active').forEach((el) => {
			el.textContent = ''
			el.classList.remove('versions__active')
		})

		document.querySelectorAll('.versions__buttons').forEach((laterEl) => {
			if (laterEl !== el) laterEl.style.width = '40px'
		})

		const blackLogo = document.querySelector('.versions__black-logotype-btn')
		blackLogo.style.left = '0'

		el.style.left = '60px'
		el.style.width = '160px'

		el.textContent = 'White logotype'
		el.classList.add('versions__active')

		/* image handling */
		document
			.querySelectorAll('.versions__image')
			.forEach((el) => (el.style.opacity = 0))

		const relativeImage = document.querySelector('#versions__white-image')
		relativeImage.style.opacity = 1

		legalText.textContent =
			'The White logotype should be used when it is placed on top of the Skanska Blue, photography or film.'
	})
)

document.querySelectorAll('.versions__black-logotype-btn').forEach((el) =>
	el.addEventListener('click', () => {
		if (el.classList.contains('versions__active')) return

		document.querySelectorAll('.versions__active').forEach((el) => {
			el.textContent = ''
			el.classList.remove('versions__active')
		})

		document.querySelectorAll('.versions__buttons').forEach((laterEl) => {
			if (laterEl !== el) laterEl.style.width = '40px'
		})

		const whiteLogo = document.querySelector('.versions__white-logotype-btn')
		whiteLogo.style.position = 'absolute'
		whiteLogo.style.left = '60px'

		el.style.left = -120 + 'px'
		el.style.width = '160px'

		el.textContent = 'Black logotype'
		el.classList.add('versions__active')

		/* image handling */
		document
			.querySelectorAll('.versions__image')
			.forEach((el) => (el.style.opacity = 0))

		const relativeImage = document.querySelector('#versions__black-image')
		relativeImage.style.opacity = 1

		legalText.textContent =
			'The Black logotype should be used when it is placed on top of the Skanska White, photography or film.'
	})
)

setTimeout(() => {
	document.body.className = ''
}, 400)
