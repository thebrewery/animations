let resizeChecker
const textContainer = document.querySelector('.hierarchy__text-containers')
const topContainer = document.querySelector('.hierarchy__first-text-block')
const topLine = document.querySelector('.hierarchy__first-line')
const middleContainer = document.querySelector('.hierarchy__second-text-block')
const middleLine = document.querySelector('.hierarchy__second-line')
const bottomContainer = document.querySelector('.hierarchy__third-text-block')
const bottomLine = document.querySelector('.hierarchy__third-line')

window.onload = () => {
	textContainer.style.height = topContainer.offsetHeight + 'px'
}

function resized() {
	resizeContainers()
}

window.onresize = function () {
	clearTimeout(resizeChecker)
	resizeChecker = setTimeout(resized, 300)
}

const resizeContainers = () => {
	const relativeTopSpan = document
		.querySelector('.hierarchy__top-trapezoid')
		.querySelector('span')
		.getBoundingClientRect()

	const relativeMiddleSpan = document
		.querySelector('.hierarchy__middle-trapezoid')
		.querySelector('span')
		.getBoundingClientRect()

	const relativeBottomSpan = document
		.querySelector('.hierarchy__bottom-trapezoid')
		.querySelector('span')
		.getBoundingClientRect()

	if (window.innerWidth >= 810) {
		topContainer.style.top = relativeTopSpan.height * 1 + 40 + 'px'
		topLine.style.top = relativeTopSpan.height * 1 + 'px'
		middleContainer.style.top = relativeMiddleSpan.height * 3.75 + 40 + 'px'
		middleLine.style.top = relativeMiddleSpan.height * 3.75 + 'px'
		bottomContainer.style.top = relativeBottomSpan.height * 5.8 + 40 + 'px'
		bottomLine.style.top = relativeBottomSpan.height * 5.8 + 'px'
		textContainer.style.height = 'auto'
	} else {
		topContainer.style.top = '0px'
		topLine.style.top = '0px'
		middleContainer.style.top = '0px'
		middleLine.style.top = '0px'
		bottomContainer.style.top = '0px'
		bottomLine.style.top = '0px'
		textContainer.style.height = topContainer.offsetHeight + 'px'
	}

	if (window.innerWidth >= 1400) {
		middleContainer.style.top = relativeMiddleSpan.height * 3.2 + 40 + 'px'
		middleLine.style.top = relativeMiddleSpan.height * 3.2 + 'px'
		bottomContainer.style.top = relativeBottomSpan.height * 5.3 + 40 + 'px'
		bottomLine.style.top = relativeBottomSpan.height * 5.3 + 'px'
	}
}

document.querySelectorAll('.hierarchy__bottom-trapezoid').forEach((el) => {
	el.addEventListener('mouseenter', () => {
		document.querySelectorAll('.hierarchy__text-container').forEach((el) => {
			el.style.opacity = 0
		})

		document
			.querySelectorAll('.hierarchy__line')
			.forEach((el) => (el.style.opacity = 0))

		const relativeSpan = el.querySelector('span')
		const rect = relativeSpan.getBoundingClientRect()

		bottomLine.style.opacity = 1
		bottomContainer.style.opacity = 1

		if (window.innerWidth >= 810) {
			bottomContainer.style.top = rect.height * 5.8 + 40 + 'px'
			bottomLine.style.top = rect.height * 5.8 + 'px'
		} else {
			bottomContainer.style.top = 0 + 'px'
		}

		if (window.innerWidth >= 1400) {
			bottomContainer.style.top = rect.height * 5.3 + 40 + 'px'
			bottomLine.style.top = rect.height * 5.3 + 'px'
		}
	})
})

document.querySelectorAll('.hierarchy__middle-trapezoid').forEach((el) => {
	el.addEventListener('mouseenter', () => {
		document.querySelectorAll('.hierarchy__text-container').forEach((el) => {
			el.style.opacity = 0
		})

		document
			.querySelectorAll('.hierarchy__line')
			.forEach((el) => (el.style.opacity = 0))

		const relativeSpan = el.querySelector('span')
		const rect = relativeSpan.getBoundingClientRect()

		middleLine.style.opacity = 1
		middleContainer.style.opacity = 1

		if (window.innerWidth >= 810) {
			middleContainer.style.top = rect.height * 3.75 + 40 + 'px'
			middleLine.style.top = rect.height * 3.75 + 'px'
		} else {
			middleContainer.style.top = 0 + 'px'
		}

		if (window.innerWidth >= 1400) {
			middleContainer.style.top = rect.height * 3.2 + 40 + 'px'
			middleLine.style.top = rect.height * 3.2 + 'px'
		}
	})
})

document.querySelectorAll('.hierarchy__top-trapezoid').forEach((el) => {
	el.addEventListener('mouseenter', () => {
		document.querySelectorAll('.hierarchy__text-container').forEach((el) => {
			el.style.opacity = 0
		})

		document
			.querySelectorAll('.hierarchy__line')
			.forEach((el) => (el.style.opacity = 0))

		const relativeSpan = el.querySelector('span')
		const rect = relativeSpan.getBoundingClientRect()

		topLine.style.opacity = 1
		topContainer.style.opacity = 1

		if (window.innerWidth >= 810) {
			topLine.style.top = rect.height * 1 + 'px'
			topContainer.style.top = rect.height * 1 + 40 + 'px'
		} else {
			topContainer.style.top = 0 + 'px'
		}
	})
})
