const brandDeviceCheck = document.querySelector(
	'input#application__brand-device'
)
const imageryCheck = document.querySelector('input#application__imagery')
const logotypeCheck = document.querySelector('input#application__logotype')
const typoCheck = document.querySelector('input#application__typography')

document.querySelectorAll('.application__checkbox-input').forEach((el) => {
	el.addEventListener('click', () => {
		document.querySelectorAll('.application__image').forEach((el) => {
			el.style.opacity = 0
			el.classList.remove('application__active')
		})

		const one = brandDeviceCheck.checked
		const two = imageryCheck.checked
		const three = logotypeCheck.checked
		const four = typoCheck.checked

		const firstColumn = document.querySelector(
			'.application__first-image-container'
		)

		const secondColumn = document.querySelector(
			'.application__second-image-container'
		)

		const thirdColumn = document.querySelector(
			'.application__third-image-container'
		)

		const fourthColumn = document.querySelector(
			'.application__fourth-image-container'
		)
		const firstColumnChildren = Array.from(firstColumn.children)
		const secondColumnChildren = Array.from(secondColumn.children)
		const thirdColumnChildren = Array.from(thirdColumn.children)
		const fourthColumnChildren = Array.from(fourthColumn.children)

		const findChild = (col, id, extension = '.jpg') => {
			const chosenChild = col.find((child) =>
				child.src.includes(id + extension)
			)
			chosenChild.style.opacity = 1
			chosenChild.classList.add('application__active')
		}

		/* one checkbox */

		if (!one && !two && !three && !four) {
			findChild(firstColumnChildren, '1_0')
			findChild(secondColumnChildren, '2_0')
			findChild(thirdColumnChildren, '3_0')
			findChild(fourthColumnChildren, '4_0')
		}

		if (one && !two && !three && !four) {
			findChild(firstColumnChildren, '1_1')
			findChild(secondColumnChildren, '2_1')
			findChild(thirdColumnChildren, '3_0')
			findChild(fourthColumnChildren, '4_0')
		}

		if (!one && two && !three && !four) {
			findChild(firstColumnChildren, '1_2')
			findChild(secondColumnChildren, '2_2')
			findChild(thirdColumnChildren, '3_2')
			findChild(fourthColumnChildren, '4_2')
		}

		if (!one && !two && three && !four) {
			findChild(firstColumnChildren, '1_3')
			findChild(secondColumnChildren, '2_3')
			findChild(thirdColumnChildren, '3_3')
			findChild(fourthColumnChildren, '4_3')
		}

		if (!one && !two && !three && four) {
			findChild(firstColumnChildren, '1_4')
			findChild(secondColumnChildren, '2_0')
			findChild(thirdColumnChildren, '3_4')
			findChild(fourthColumnChildren, '4_4')
		}

		/* two checkbox */

		if (one && two && !three && !four) {
			findChild(firstColumnChildren, '1_1-2')
			findChild(secondColumnChildren, '2_1-2')
			findChild(thirdColumnChildren, '3_2')
			findChild(fourthColumnChildren, '4_2')
		}

		if (one && !two && three && !four) {
			findChild(firstColumnChildren, '1_1-3')
			findChild(secondColumnChildren, '2_1-3')
			findChild(thirdColumnChildren, '3_3')
			findChild(fourthColumnChildren, '4_3')
		}

		if (one && !two && !three && four) {
			findChild(firstColumnChildren, '1_1-4')
			findChild(secondColumnChildren, '2_1')
			findChild(thirdColumnChildren, '3_4')
			findChild(fourthColumnChildren, '4_4')
		}

		if (!one && two && three && !four) {
			findChild(firstColumnChildren, '1_2-3')
			findChild(secondColumnChildren, '2_2-3')
			findChild(thirdColumnChildren, '3_2-3')
			findChild(fourthColumnChildren, '4_2-3')
		}

		if (!one && two && !three && four) {
			findChild(firstColumnChildren, '1_2-4')
			findChild(secondColumnChildren, '2_2')
			findChild(thirdColumnChildren, '3_2-4')
			findChild(fourthColumnChildren, '4_2-4')
		}

		if (!one && !two && three && four) {
			findChild(firstColumnChildren, '1_3-4')
			findChild(secondColumnChildren, '2_3')
			findChild(thirdColumnChildren, '3_3-4')
			findChild(fourthColumnChildren, '4_3-4')
		}

		/* three checkboxes */
		if (one && two && three && !four) {
			findChild(firstColumnChildren, '1_01', '.png')
			findChild(firstColumnChildren, '1_02', '.png')
			findChild(firstColumnChildren, '1_03', '.png')
			findChild(secondColumnChildren, '2_1-2-3')
			findChild(thirdColumnChildren, '3_2-3')
			findChild(fourthColumnChildren, '4_2-3')
		}

		if (one && two && !three && four) {
			findChild(firstColumnChildren, '1_01', '.png')
			findChild(firstColumnChildren, '1_02', '.png')
			findChild(firstColumnChildren, '1_04', '.png')
			findChild(secondColumnChildren, '2_1-2')
			findChild(thirdColumnChildren, '3_2-4')
			findChild(fourthColumnChildren, '4_2-4')
		}

		if (one && !two && three && four) {
			findChild(firstColumnChildren, '1_01', '.png')
			findChild(firstColumnChildren, '1_03', '.png')
			findChild(firstColumnChildren, '1_04', '.png')
			findChild(secondColumnChildren, '2_1-3')
			findChild(thirdColumnChildren, '3_3-4')
			findChild(fourthColumnChildren, '4_3-4')
		}

		if (!one && two && three && four) {
			findChild(firstColumnChildren, '1_02', '.png')
			findChild(firstColumnChildren, '1_03', '.png')
			findChild(firstColumnChildren, '1_04', '.png')
			findChild(secondColumnChildren, '2_2-3')
			findChild(thirdColumnChildren, '3_2-3-4')
			findChild(fourthColumnChildren, '4_2-3-4')
		}

		if (one && two && three && four) {
			findChild(firstColumnChildren, '1_1-2-3-4')
			findChild(secondColumnChildren, '2_1-2-3')
			findChild(thirdColumnChildren, '3_2-3-4')
			findChild(fourthColumnChildren, '4_2-3-4')
		}
	})
})
