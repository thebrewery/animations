document.querySelectorAll('.size__radio-button').forEach((el) => {
	el.addEventListener('click', () => {
		document
			.querySelectorAll('.size__text-selection')
			.forEach((textContainer) => {
				if (textContainer.classList.contains(el.id)) {
					textContainer.style.display = 'block'
					textContainer.classList.add('size__active')

					setTimeout(() => {
						textContainer.style.opacity = 1
					}, 50)
				} else {
					textContainer.style.display = 'none'
					textContainer.style.opacity = 0
					textContainer.classList.remove('size__active')
				}
			})
	})
})
