window.onload = () => {
	var x, i, resizeChecker, hasAnimated

	// Attach this classname to the second image, contained in the same container as it's counterpart
	x = document.getElementsByClassName('comparison__overlay')

	for (i = 0; i < x.length; i++) {
		// This calls the function for all elements containing the comparison__overlay classname
		triggerComparison(x[i])

		// This makes it so animation for slider, doesn't retrigger with resizing
		if (x.length === i + 1 && !hasAnimated) {
			hasAnimated = true
		}
	}

	function resized() {
		for (i = 0; i < x.length; i++) {
			// This calls the function for all elements containing the comparison__overlay classname
			triggerComparison(x[i])
		}
	}

	window.onresize = function () {
		clearTimeout(resizeChecker)
		resizeChecker = setTimeout(resized, 500)
	}

	function triggerComparison(img) {
		img.parentElement
			.querySelectorAll('.comparison__slider')
			.forEach((el) => el.remove())

		var slider,
			clicked = 0,
			width = img.offsetWidth,
			height = img.offsetHeight,
			comparisonContainer = img.parentElement

		img.style.width = comparisonContainer.offsetWidth / 2 + 'px'

		//	This creates the slider, and appends it inbetween the two images.
		slider = document.createElement('img')
		slider.src = './handle.svg'
		slider.setAttribute('class', 'comparison__slider')

		if (!hasAnimated) {
			slider.classList.add('animateSlider')
		}

		img.classList.add('animateImg')
		img.parentElement.insertBefore(slider, img)

		slider.style.top = height * 0.7 + 'px'
		slider.style.left =
			comparisonContainer.offsetWidth / 2 - slider.offsetWidth / 2 + 'px'

		// Fires an event when the mouse button is pressed/released/touched
		slider.addEventListener('mousedown', triggerSlider)
		slider.addEventListener('touchstart', triggerSlider)
		window.addEventListener('mouseup', exitSlider)
		window.addEventListener('touchend', exitSlider)

		function triggerSlider(e) {
			e.preventDefault()
			clicked = 1

			// This fires the event, that causes the slider/overlay to move
			window.addEventListener('mousemove', moveComparisonElements)
			window.addEventListener('touchmove', moveComparisonElements)
		}

		function exitSlider() {
			clicked = 0
		}

		function moveComparisonElements(event) {
			if (clicked == 0) return false

			var pos

			pos = getCursorPos(event)

			if (pos < 0) pos = 0
			if (pos > comparisonContainer.offsetWidth)
				pos = comparisonContainer.offsetWidth

			slide(pos)
		}

		function getCursorPos(event) {
			var xValue = 0
			event = event.changedTouches ? event.changedTouches[0] : event

			xValue = event.pageX - img.getBoundingClientRect().left
			xValue = xValue - window.pageXOffset

			return xValue
		}

		function slide(xValue) {
			img.style.width = xValue + 'px'
			slider.style.left = img.offsetWidth - slider.offsetWidth / 2 + 'px'
		}
	}
}
