document.querySelectorAll('#bdc__landscape').forEach((el) =>
	el.addEventListener('click', () => {
		document.querySelectorAll('.bdc__word').forEach((el) => {
			el.style.opacity = 0
			el.classList.remove('bdc__active')
		})

		const relative = document.querySelector(`#${el.id}_img`)
		relative.style.opacity = 1
		relative.classList.add('bdc__active')
	})
)

document.querySelectorAll('#bdc__portrait').forEach((el) =>
	el.addEventListener('click', () => {
		document.querySelectorAll('.bdc__word').forEach((el) => {
			el.style.opacity = 0
			el.classList.remove('bdc__active')
		})

		const relative = document.querySelector(`#${el.id}_img`)
		relative.style.opacity = 1
		relative.classList.add('bdc__active')
	})
)

document.querySelectorAll('#bdc__square').forEach((el) =>
	el.addEventListener('click', () => {
		document.querySelectorAll('.bdc__word').forEach((el) => {
			el.style.opacity = 0
			el.classList.remove('bdc__active')
		})

		const relative = document.querySelector(`#${el.id}_img`)
		relative.style.opacity = 1
		relative.classList.add('bdc__active')
	})
)

document.querySelectorAll('#bdc__square-alt').forEach((el) =>
	el.addEventListener('click', () => {
		document.querySelectorAll('.bdc__word').forEach((el) => {
			el.style.opacity = 0
			el.classList.remove('bdc__active')
		})

		const relative = document.querySelector(`#${el.id}_img`)
		relative.style.opacity = 1
		relative.classList.add('bdc__active')
	})
)
